2. Edit the ``/etc/supply/supply.conf`` file and complete the following
   actions:

   * In the ``[database]`` section, configure database access:

     .. code-block:: ini

        [database]
        ...
        connection = mysql+pymysql://supply:SUPPLY_DBPASS@controller/supply
