Prerequisites
-------------

Before you install and configure the supply service,
you must create a database, service credentials, and API endpoints.

#. To create the database, complete these steps:

   * Use the database access client to connect to the database
     server as the ``root`` user:

     .. code-block:: console

        $ mysql -u root -p

   * Create the ``supply`` database:

     .. code-block:: none

        CREATE DATABASE supply;

   * Grant proper access to the ``supply`` database:

     .. code-block:: none

        GRANT ALL PRIVILEGES ON supply.* TO 'supply'@'localhost' \
          IDENTIFIED BY 'SUPPLY_DBPASS';
        GRANT ALL PRIVILEGES ON supply.* TO 'supply'@'%' \
          IDENTIFIED BY 'SUPPLY_DBPASS';

     Replace ``SUPPLY_DBPASS`` with a suitable password.

   * Exit the database access client.

     .. code-block:: none

        exit;

#. Source the ``admin`` credentials to gain access to
   admin-only CLI commands:

   .. code-block:: console

      $ . admin-openrc

#. To create the service credentials, complete these steps:

   * Create the ``supply`` user:

     .. code-block:: console

        $ openstack user create --domain default --password-prompt supply

   * Add the ``admin`` role to the ``supply`` user:

     .. code-block:: console

        $ openstack role add --project service --user supply admin

   * Create the supply service entities:

     .. code-block:: console

        $ openstack service create --name supply --description "supply" supply

#. Create the supply service API endpoints:

   .. code-block:: console

      $ openstack endpoint create --region RegionOne \
        supply public http://controller:XXXX/vY/%\(tenant_id\)s
      $ openstack endpoint create --region RegionOne \
        supply internal http://controller:XXXX/vY/%\(tenant_id\)s
      $ openstack endpoint create --region RegionOne \
        supply admin http://controller:XXXX/vY/%\(tenant_id\)s
