===============================
supply
===============================

Supply does baremetal provisioning of your servers. Its goal is to be a
general-purpose deployment system to install any kind of clusters. Even
if it uses OpenStack libraries, it isn't bound to install OpenStack only,
and can be used to deploy any kind of workload on your baremetal systems.

* Free software: Apache license
* Documentation: https://docs.openstack.org/supply/latest
* Source: https://opendev.org/openstack/supply
* Bugs: https://bugs.launchpad.net/supply

Features
--------

* TODO
