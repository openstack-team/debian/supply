class supply::sysctl(
){
  # Increase conntrack
  # Note: need to load the nf_conntrack module
  # BEFORE doing the sysctl, otherwise, puppet cannot
  # apply changes.
  # Also note that we're scheduling this a long time
  # before tweaking any net.netfilter.nf_conntrack*
  # as it takes some time to load the conntrack module,
  # and this sysctl may otherwise fail (race condition).
  kmod::load { 'nf_conntrack': }
  # As few swap as possible
  -> sysctl::value { 'vm.swappiness':
    value  => '1',
    target => '/etc/sysctl.d/50-vm-swappiness.conf',
  }

  # Allow binding a socket on an IP which isn't configured
  # in the server just yet.
  -> sysctl::value { 'net.ipv4.ip_nonlocal_bind':
    value => "1",
    target => '/etc/sysctl.d/ip-nonlocal-bind.conf',
  }

  # Increase the max number of sockets
  -> sysctl::value { 'net.core.somaxconn':
    value  => '65536',
    target => '/etc/sysctl.d/20-somaxconn.conf',
  }
  # Setup some useful sysctl customization
  -> sysctl::value { 'net.ipv4.neigh.default.gc_thresh1':
    value  => '4096',
    target => '/etc/sysctl.d/40-ipv4-neigh-1.conf',
  }

  -> sysctl::value { 'net.ipv4.neigh.default.gc_thresh2':
    value  => '8192',
    target => '/etc/sysctl.d/40-ipv4-neigh-2.conf',
  }

  -> sysctl::value { 'net.ipv4.neigh.default.gc_thresh3':
    value  => '16384',
    target => '/etc/sysctl.d/40-ipv4-neigh-3.conf',
  }

  # Same for IPv6
  -> sysctl::value { 'net.ipv6.neigh.default.gc_thresh1':
    value  => '4096',
    target => '/etc/sysctl.d/40-ipv6-neigh-1.conf',
  }

  -> sysctl::value { 'net.ipv6.neigh.default.gc_thresh2':
    value  => '8192',
    target => '/etc/sysctl.d/40-ipv6-neigh-2.conf',
  }

  -> sysctl::value { 'net.ipv6.neigh.default.gc_thresh3':
    value  => '16384',
    target => '/etc/sysctl.d/40-ipv6-neigh-3.conf',
  }

  -> sysctl::value { 'net.netfilter.nf_conntrack_max':
    value  => '2621440',
    target => '/etc/sysctl.d/40-nf-conntrack-max-1.conf',
  }
  -> sysctl::value { 'net.nf_conntrack_max':
    value  => '2621440',
    target => '/etc/sysctl.d/40-nf-conntrack-max-2.conf',
  }

}
