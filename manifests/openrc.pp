# == Parameters:
#
# [*name*]
#   (Required) The name of the OpenStack user to write openrc file for.
#
# [*username*]
#   (Required) The name of the service user
#
# [*password*]
#   (Required) Password to create for the service user
#
# [*auth_url*]
#   (Required) The URL to use for authentication.
#
# [*project_name*]
#   (Optional) Service project name
#   Defaults to $::os_service_default
#
define supply::openrc(
  $username             = 'admin',
  $project_name         = 'admin',
  $password,
  $auth_fqdn            = 'localhost',
  $auth_port            = '443',
  $auth_proto           = 'https',
  $region_name          = 'RegionOne',
  $api_endpoint_ca_file = undef,
){

  if $auth_port == '443' and $auth_proto == 'https' {
    $auth_port_real = ''
  }else{
    $auth_port_real = ":${auth_port}"
  }

  ensure_resource('file', "/root/${name}-openrc", {
    'ensure'  => 'present',
    'content' => "
export OS_AUTH_TYPE=password
export OS_PROJECT_DOMAIN_NAME='default'
export OS_USER_DOMAIN_NAME='default'
export OS_PROJECT_NAME='${project_name}'
export OS_USERNAME='${username}'
export OS_PASSWORD='${password}'
export OS_AUTH_URL='${auth_proto}://${auth_fqdn}${auth_port_real}/identity/v3'
export OS_REGION_NAME=${region_name}
export OS_IDENTITY_API_VERSION=3
export OS_IMAGE_API_VERSION=2
export OS_CACERT=${$api_endpoint_ca_file}
export IRONIC_API_VERSION=1.78
export OS_BAREMETAL_API_VERSION=1.78
",
    'mode'    => '0640',
  })

}
