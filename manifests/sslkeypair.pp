define supply::sslkeypair(
  $notify_service_name = 'httpd',
  $require_anchor      = undef,
){

    if $require_anchor {
      $require_anchor_real = $require_anchor
    }else{
      $require_anchor_real = Anchor["${name}::install::end"]
    }

    File['/etc/ssl/private/supply-pki-api.key'] -> File["/etc/${name}/ssl/private/${::fqdn}.pem"]
    File['/etc/ssl/certs/supply-pki-api.crt']   -> File["/etc/${name}/ssl/public/${::fqdn}.crt"]

    file { "/etc/${name}/ssl":
      ensure                  => directory,
      owner                   => 'root',
      mode                    => '0755',
      selinux_ignore_defaults => true,
      require                 => $require_anchor_real,
    }->
    file { "/etc/${name}/ssl/private":
      ensure                  => directory,
      owner                   => 'root',
      mode                    => '0755',
      selinux_ignore_defaults => true,
    }->
    file { "/etc/${name}/ssl/public":
      ensure                  => directory,
      owner                   => 'root',
      mode                    => '0755',
      selinux_ignore_defaults => true,
    }->
    file { "/etc/${name}/ssl/private/${::fqdn}.pem":
      ensure                  => present,
      owner                   => "${name}",
      source                  => "/etc/ssl/private/ssl-cert-snakeoil.key",
      selinux_ignore_defaults => true,
      mode                    => '0600',
    }->
    file { "/etc/${name}/ssl/public/${::fqdn}.crt":
      ensure                  => present,
      owner                   => "${name}",
      source                  => '/etc/ssl/certs/ssl-cert-snakeoil.pem',
      selinux_ignore_defaults => true,
      mode                    => '0644',
      notify                  => Service[$notify_service_name],
    }
}
