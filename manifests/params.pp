# == Class: supply::params
#
# These parameters need to be accessed from several locations and
# should be considered to be constant
class supply::params {
  include openstacklib::defaults

  $common_package_name = 'supply-common'
  $python_package_name = 'python3-supply'
}
