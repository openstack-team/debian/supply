class supply::services::nova::common(
  $machine_ip                        = undef,

  $pass_nova_messaging               = undef,
  $pass_nova_db                      = undef,
  $pass_novaapi_db                   = undef,
  $pass_nova_authtoken               = undef,
  $pass_nova_ssh_pub                 = undef,
  $pass_nova_ssh_priv                = undef,

  $pass_metadata_proxy_shared_secret = undef,
  $dhcp_domain                       = 'example.com',

  $pass_neutron_authtoken            = undef,
  $pass_placement_authtoken          = undef,

  $controller_ips                    = [],
  $rabbit_hostnames                  = [],
  $sql_host                          = undef,
  $api_fqdn                          = undef,
  $region_name                       = undef,
){

  class { '::nova::logging':
    debug => true,
  }

  class { '::nova::glance':
    endpoint_override => "https://${api_fqdn}/image",
  }

  class { '::nova::keystone::authtoken':
    password             => $pass_nova_authtoken,
    auth_url             => "https://${api_fqdn}/identity",
    www_authenticate_uri => "https://${api_fqdn}/identity",
    memcached_servers    => $memcached_servers,
    region_name          => $region_name,
  }

  class { '::nova::placement':
    password             => $pass_placement_authtoken,
    auth_url             => "https://${api_fqdn}/identity",
    region_name          => $region_name,
  }

  class { '::nova':
    default_transport_url      => os_transport_url({
      'transport' => 'rabbit',
      'hosts'     => fqdn_rotate($rabbit_hostnames),
      'port'      => '5671',
      'username'  => 'nova',
      'password'  => $pass_nova_messaging,
    }),
#    notification_transport_url    => $nova_notif_transport_url,
    rabbit_use_ssl                => true,
    rabbit_ha_queues              => true,
#    kombu_ssl_ca_certs            => $oci_pki_root_ca_file,
    amqp_sasl_mechanisms          => 'PLAIN',
#    notification_driver           => $notification_driver,
    notification_driver           => 'messagingv2',
    notify_on_state_change        => 'vm_and_task_state',

    nova_public_key        => { type => 'ssh-rsa', key => $pass_nova_ssh_pub },
    nova_private_key       => { type => 'ssh-rsa', key => base64('decode', $pass_nova_ssh_priv) },
# TODO: Make it configurable
#    cpu_allocation_ratio   => $cpu_allocation_ratio,
    cpu_allocation_ratio   => 1,
    ram_allocation_ratio   => 1,
    disk_allocation_ratio  => 1,
  }
  nova_config {
    'glance/api_servers': value => "https://${api_fqdn}/image";
  }

  class { '::nova::network::neutron':
    auth_url              => "https://${api_fqdn}/identity/v3",
    password              => $pass_neutron_authtoken,
    default_floating_pool => 'public',
    endpoint_override     => "https://${api_fqdn}/network",
    region_name           => $region_name,
  }
  class { '::nova::key_manager::barbican':
    barbican_endpoint => "https://${api_fqdn}/keymanager",
    auth_endpoint     => "https://${api_fqdn}/identity/v3",
  }
}
