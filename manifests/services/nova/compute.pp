class supply::services::nova::compute(
  $machine_ip                   = undef,
  $api_fqdn                     = undef,
#  $setup_controller             = false,
  $controller_ips               = [],
#  $controller_hostnames         = [],

#  $use_dvr                      = false,
  $disable_notifications        = false,

  $nova_reserved_host_memory_mb = 16384,

  $pass_ironic_authtoken        = undef,

  $backend_type                 = 'ironic',
  $region_name                  = 'RegionOne',
){

# TODO: Setup haproxy for the metadataproxy
# when doing multi-node stuff.

#  if $setup_controller == false {
#    if $use_dvr == true {
#      haproxy::frontend { 'metadataproxyfe':
#        mode      => 'http',
#        bind      => { "127.0.0.1:8775" => [] },
#        options   => [
#          { 'use_backend' => 'metadatabe' },
#        ]
#      }
#      haproxy::balancermember { 'metadatabm':
#        listening_service => 'metadatabe',
#        ipaddresses       => $controller_ips,
#        server_names      => $controller_hostnames,
#        ports             => 8775,
#        options           => 'check check-ssl ssl verify'
#      }
#    }
#  }

  # This is needed for Buster, so live migration can work
  package { 'apparmor':
    ensure => present,
    before => Class['::nova'],
  }

  if $backend_type == 'libvirt' {
    # This enables live migrations over libvirt's TLS support
    class { '::nova::migration::libvirt':
      transport                      => 'tls',
      live_migration_inbound_addr    => $::fqdn,
      live_migration_tunnelled       => false,
      live_migration_with_native_tls => true,
      ca_file                        => '/etc/pki/CA/cacert.pem',
      modular_libvirt                => true,
    }

    class { '::nova::compute::libvirt':
      disk_cachemodes                            => $disk_cachemodes,
      virt_type                                  => $virt_type,
      cpu_mode                                   => $cpu_mode_real,
      cpu_models                                 => [ $cpu_model_real, ],
      cpu_model_extra_flags                      => $cpu_model_extra_flags,
      migration_support                          => true,
      # False is needed because of the above include ::nova::compute::libvirt::services
      manage_libvirt_services                    => false,
      preallocate_images                         => 'space',
      # This is one week retention.
      vncserver_listen                           => '0.0.0.0',
      hw_disk_discard                            => 'unmap',
    }

    create_resources( 'libvirtd_config', {
      'key_file'=>  { 'value' => '/etc/pki/libvirt/private/serverkey.pem', 'quote'  => true },
      'cert_file'=> { 'value' => '/etc/pki/libvirt/servercert.pem', 'quote'  => true },
    })

    # The +0 is there for converting the string to an int,
    # 1536 is for the PoC, 8192 is for real deployments where we do expect
    # hosts to hold more than 16GB of RAM.
    if (($::memorysize_mb + 0) < 16000) {
      $reserved_host_memory_mb = 4096
    }else{
      $reserved_host_memory_mb = $nova_reserved_host_memory_mb
    }

    nova_config {
      'DEFAULT/reserved_host_disk_mb':    value => $nova_reserved_host_disk_mb;
    }

    file_line { 'parallel-shutdown-of-vms':
      path    => '/etc/default/libvirt-guests',
      match   => '.*PARALLEL_SHUTDOWN.*=.*',
      line    => 'PARALLEL_SHUTDOWN=8',
      require => Class['::nova::compute::libvirt'],
    }->
    file_line { 'shutdown-timeout-of-vms':
      path   => '/etc/default/libvirt-guests',
      match  => '.*SHUTDOWN_TIMEOUT.*=.*',
      line   => 'SHUTDOWN_TIMEOUT=120',
    }->
    file_line { 'start-delay-of-vms':
      path   => '/etc/default/libvirt-guests',
      match  => '.*START_DELAY.*=.*',
      line   => 'START_DELAY=4',
    }->
    file_line { 'use-apparmor-not-selinux':
      path   => '/etc/libvirt/qemu.conf',
      match  => '^#?security_driver.*=.*',
      line   => $libvirt_security_driver_content,
      notify => Service['libvirtd'],
    }->
    file_line { 'enable-vnc-tls':
      path   => '/etc/libvirt/qemu.conf',
      match  => '^#?vnc_tls[ \t]*=.*',
      line   => 'vnc_tls=1',
      notify => Service['libvirtd'],
    }->
    file_line { 'vnc-tls-x509-verify':
      path   => '/etc/libvirt/qemu.conf',
      match  => '^#?vnc_tls_x509_verify[ \t]*=.*',
      line   => 'vnc_tls_x509_verify=1',
      notify => Service['libvirtd'],
    }

    class { '::nova::compute':
      vnc_enabled                      => true,
      vncproxy_host                    => "${api_fqdn}",
      vncproxy_protocol                => 'https',
      vncproxy_port                    => '443',
      vncproxy_path                    => '/novnc/vnc_auto.html',
      vncserver_proxyclient_address    => $machine_ip,
      instance_usage_audit             => true,
      instance_usage_audit_period      => 'hour',
      resume_guests_state_on_host_boot => true,
      force_config_drive               => true,
      reserved_host_memory             => $reserved_host_memory_mb,
      reserved_host_disk               => 1,
      use_cow_images                   => 'False',
      default_ephemeral_format         => 'ext4',
    }

  }else{
    class { '::nova::ironic::common':
      auth_url             => "https://${api_fqdn}/identity",
      password             => $pass_ironic_authtoken,
      project_name         => 'services',
      username             => 'ironic',
      endpoint_override    => "https://${api_fqdn}/baremetal",
      region_name          => $region_name,
#      service_type         => $::os_service_default,
#      valid_interfaces     => $::os_service_default,
#      timeout              => $::os_service_default,
    }

    class { '::nova::compute::ironic': }
    package { 'nova-compute-ironic':
      ensure => present,
      notify => Service['nova-compute'],
    }
    -> class { '::nova::compute':
      vnc_enabled              => false,
      spice_enabled            => false,
      force_config_drive       => true,
      reserved_host_memory     => 0,
#      reserved_host_disk       => 1,
#      use_cow_images           => 'False',
#      default_ephemeral_format => 'ext4',
      ensure_package           => false,
    }
  }
  class { '::nova::compute::image_cache':
    remove_unused_original_minimum_age_seconds => '604800',
  }

}
