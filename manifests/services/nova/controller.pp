class supply::services::nova::controller(
  $machine_ip          = undef,
  $sql_host            = undef,
  $api_fqdn            = undef,

  $pass_nova_messaging = undef,
  $pass_nova_db        = undef,
  $pass_novaapi_db     = undef,
  $pass_nova_authtoken = undef,

  $region_name         = 'RegionOne',
){

  ::supply::services::mysql::user { 'nova': mysql_password => $pass_nova_db }
  class { '::nova::db::mysql_api':
    dbname        => 'novaapidb',
    user          => 'novaapi',
    password      => $pass_novaapi_db,
    allowed_hosts => '%',
    charset       => 'utf8mb3',
    collate       => 'utf8mb3_general_ci',
    require       => Anchor['mysql::server::end'],
    before        => Anchor["nova::service::begin"],
  }

  ::supply::sslkeypair {'nova': notify_service_name => 'nova-api' }

  class { '::nova::cell_v2::simple_setup':
    require => Exec['nova-db-sync-api'],
  }
  Class['nova'] -> Class['::nova::cell_v2::simple_setup']

  class { '::nova::db':
    database_connection              => "mysql+pymysql://nova:${pass_nova_db}@${sql_host}/novadb?charset=utf8",
    api_database_connection          => "mysql+pymysql://novaapi:${pass_novaapi_db}@${sql_host}/novaapidb?charset=utf8",
    database_connection_recycle_time => 1800,
  }

  class { '::nova::keystone::auth':
    public_url   => "https://${api_fqdn}/compute/v2.1",
    internal_url => "https://${api_fqdn}/compute/v2.1",
    admin_url    => "https://${api_fqdn}/compute/v2.1",
    password     => $pass_nova_authtoken,
    region       => $region_name,
  }

  class { '::nova::api':
    api_bind_address                     => $machine_ip,
    sync_db                              => true,
    sync_db_api                          => true,
    allow_resize_to_same_host            => true,
    enable_proxy_headers_parsing         => true,
    max_limit                            => 10000,
  }
  class { '::nova::metadata':
    neutron_metadata_proxy_shared_secret => $pass_metadata_proxy_shared_secret,
    dhcp_domain                          => $dhcp_domain,
    metadata_cache_expiration            => $nova_metadata_cache_expiration,
  }
  # Fix the number of uwsgi processes
  class { '::nova::wsgi::uwsgi_api': }
  class { '::nova::wsgi::uwsgi_api_metadata': }

  # Disable uwsgi logs
  nova_api_uwsgi_config {
    'uwsgi/disable-logging': value => true;
  }
  nova_api_metadata_uwsgi_config {
    'uwsgi/disable-logging': value => true;
  }
  nova_config {
    'glance/region_name': value => $region_name;
  }
  class { '::nova::client': }
  class { '::nova::conductor':
    workers             => $::os_workers,
    enable_new_services => false, # Disable new compute by default
  }
  class { '::nova::cron::archive_deleted_rows': }

  class { '::nova::scheduler':
    workers                              => $::os_workers,
    limit_tenants_to_placement_aggregate => $nova_limit_tenants_to_placement_aggregate,
    discover_hosts_in_cells_interval     => 120,
  }
  class { '::nova::scheduler::filter':
    ram_weight_multiplier           => $ram_weight_multiplier,
    track_instance_changes          => false,
    shuffle_best_same_weighed_hosts => true,
  }

  class { '::nova::vncproxy':
    host           => $machine_ip,
    vncproxy_path  => "/novnc/vnc_auto.html",
    allow_noauth   => true,
    allow_vencrypt => true,
    vencrypt_ca    => '/etc/pki/nova-novncproxy/ca-cert.pem',
    vencrypt_key   => '/etc/pki/nova-novncproxy/client-key.pem',
    vencrypt_cert  => '/etc/pki/nova-novncproxy/client-cert.pem',
  }
  class { '::nova::vncproxy::common':
      vncproxy_protocol => 'https',
      vncproxy_host     => "${vip_hostname}",
      vncproxy_path     => "/novnc/vnc_auto.html",
      vncproxy_port     => "443",
  }
}
