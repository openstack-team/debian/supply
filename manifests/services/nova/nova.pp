class supply::services::nova::nova(
  $machine_ip              = undef,
  $machine_hostname        = undef,
  $region_name             = 'RegionOne',
  $pass_nova_keystoneauth  = undef,
  $pass_nova_messaging     = undef,
  $pass_nova_db            = undef,
  $pass_nova_api_db        = undef,
  $pass_nova_ssh_pub       = undef,
  $pass_nova_ssh_priv      = undef,
  $api_fqdn                = undef,
  $setup_controller        = false,
  $setup_compute           = false,
  $vip_ip                  = undef,
  $controller_ips          = [],
  $controller_hostnames    = [],
  $bus_messaging_servers   = [],
  $notif_messaging_servers = [],
  $use_dvr                 = false,
  $disable_notifications   = false,
  $cpu_allocation_ratio    = 1,
  $ram_allocation_ratio    = 1,
  $disk_allocation_ratio   = 1,
  $pki_root_ca_file        = undef,
){

  if $disable_notifications {
    $nova_notif_transport_url = ''
  } else {
    $nova_notif_transport_url = os_transport_url({
                                  'transport' => 'rabbit',
                                  'hosts'     => fqdn_rotate($notif_rabbit_servers),
                                  'port'      => '5672',
                                  'username'  => 'nova',
                                  'password'  => $pass_nova_messaging,
                                })
  }
  $nova_transport_url = os_transport_url({
      'transport' => 'rabbit',
      'hosts'     => fqdn_rotate($bus_messaging_servers),
      'port'      => '5672',
      'username'  => 'nova',
      'password'  => $pass_nova_messaging,
    })

  class { '::nova':
    default_transport_url      => $nova_transport_url,
    notification_transport_url => $nova_notif_transport_url,
    rabbit_use_ssl             => true,
    rabbit_ha_queues           => true,
    kombu_ssl_ca_certs         => $pki_root_ca_file,
    amqp_sasl_mechanisms       => 'PLAIN',
    notification_driver        => $notification_driver,
    notify_on_state_change     => 'vm_and_task_state',
    nova_public_key            => { type => 'ssh-rsa', key => $pass_nova_ssh_pub },
    nova_private_key           => { type => 'ssh-rsa', key => base64('decode', $pass_nova_ssh_priv) },
    cpu_allocation_ratio       => $cpu_allocation_ratio,
    ram_allocation_ratio       => $ram_allocation_ratio,
    disk_allocation_ratio      => $disk_allocation_ratio,
  }

  if $setup_controller{
    class {'supply::manifests::services::nova::controller':
      machine_ip             => $machine_ip,
      machine_hostname       => $machine_hostname,
      region_name            => $region_name,
      pass_nova_keystoneauth => $pass_nova_keystoneauth,
      pass_nova_messaging     => $pass_nova_messaging,
      pass_nova_db           => $pass_nova_db,
      pass_nova_api_db       => $pass_nova_api_db,
      api_fqdn               => $api_fqdn,
    }
  }
  if $setup_compute{
    class {'supply::manifests::services::nova::compute':
      machine_ip             => $machine_ip,
      machine_hostname       => $machine_hostname,
      region_name            => $region_name,
      pass_nova_keystoneauth => $pass_nova_keystoneauth,
      pass_nova_messaging    => $pass_nova_messaging,
      api_fqdn               => $api_fqdn,
      vip_ip                 => $vip_ip,
      controller_ips         => $controller_ips,
      controller_hostnames   => $controller_hostnames,
    }
  }


}
