class supply::services::memcached(
  $machine_ip = undef,
){
  class { '::memcached':
    listen_ip       => $machine_ip,
    udp_port        => 0,
    max_memory      => '20%',
    max_connections => 16384,
    pidfile         => '/var/run/memcached/memcached.pid',
    user            => 'memcache',
  }
}
