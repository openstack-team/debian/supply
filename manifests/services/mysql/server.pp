class supply::services::mysql::server(
  $machine_ip           = undef,
  $machine_hostname     = undef,

  $first_master_ip      = undef,

  $pass_mysql_rootuser  = undef,
  $pass_mysql_backup    = undef,

  $controller_ips       = [],
  $controller_hostnames = [],
){
  package { 'mariadb-backup':
    ensure => present,
    before => Class['mysql::server'],
  }

  $ram_total_in_mb = Integer($facts['memorysize_mb'])
  $ram_total_in_gb = $ram_total_in_mb / 1024
  $ram_total_in_gb_div_4 = $ram_total_in_gb / 4

  if $ram_total_in_gb_div_4 > 64 {
    $innodb_buffer_pool_size = 64
  }elsif $ram_total_in_gb_div_4 > 32{
    $innodb_buffer_pool_size = 32
  }elsif $ram_total_in_gb_div_4 > 16{
    $innodb_buffer_pool_size = 16
  }elsif $ram_total_in_gb_div_4 > 8{
    $innodb_buffer_pool_size = 8
  }elsif $ram_total_in_gb_div_4 > 4{
    $innodb_buffer_pool_size = 4
  }else{
    $innodb_buffer_pool_size = 2
  }

  class { '::mysql::client':
    package_name   => 'default-mysql-client',
  }

  -> class { '::mysql::server':
    package_name          => 'mariadb-server',
    root_password         => $pass_mysql_rootuser,
    override_options => {
      'mysqld' => {
        'bind_address'                    => $machine_ip,
        'wait_timeout'                    => '28800',
        'interactive_timeout'             => '30',
        'connect_timeout'                 => '30',
        'character_set_server'            => 'utf8',
        'collation_server'                => 'utf8_general_ci',
        'innodb_buffer_pool_size'         => "${innodb_buffer_pool_size}G",
        'innodb_flush_log_at_trx_commit'  => '2',
        'max_connections'                 => '5000',
        'max_user_connections'            => '1000',
        'binlog_cache_size'               => '1M',
        'log-bin'                         => 'mysql-bin',
        'binlog_format'                   => 'ROW',
        'performance_schema'              => '1',
        'log_warnings'                    => '2',
        'wsrep_sst_auth'                  => "backup:${pass_mysql_backup}",
        'wsrep_sst_method'                => 'mariabackup',
        'wsrep_cluster_name'              => $cluster_name,
        'wsrep_node_name'                 => $machine_hostname,
        'wsrep_provider_options'          => 'cert.log_conflicts=YES;gcache.recover=yes;gcache.size=5G',
      }
    }
  }
}
