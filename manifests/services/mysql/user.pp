define supply::services::mysql::user(
  $mysql_password = undef,
){
  class { "::${name}::db::mysql":
    dbname        => "${name}db",
    password      => $mysql_password,
    allowed_hosts => '%',
    charset       => 'utf8mb3',
    collate       => 'utf8mb3_general_ci',
    require       => Anchor['mysql::server::end'],
    before        => Anchor["${name}::service::begin"],
  }
}
