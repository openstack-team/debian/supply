class supply::services::barbican(
  $pass_barbican_messaging = undef,
  $pass_barbican_db        = undef,
  $pass_barbican_authtoken = undef,

  $controller_ips          = [],
  $rabbit_hostnames        = [],
  $sql_host                = undef,
  $api_fqdn                = undef,
  $region_name             = undef,
){

  ::supply::services::mysql::user { 'barbican': mysql_password => $pass_barbican_db }

  ::supply::sslkeypair {'barbican':
    notify_service_name => 'barbican-api',
  }

  class { '::barbican::db':
    database_connection              => "mysql+pymysql://barbican:${pass_barbican_db}@${sql_host}/barbicandb?charset=utf8",
    database_connection_recycle_time => 1800,
  }

  class { '::barbican::keystone::auth':
    public_url   => "https://${api_fqdn}/keymanager",
    internal_url => "https://${api_fqdn}/keymanager",
    admin_url    => "https://${api_fqdn}/keymanager",
    password     => $pass_barbican_authtoken,
    region       => $region_name,
  }

  include ::barbican::quota
  include ::barbican::keystone::notification
  class { '::barbican::api::logging':
    debug => true,
  }

  class { '::barbican::keystone::authtoken':
    password             => $pass_barbican_authtoken,
    auth_url             => "https://${api_fqdn}/identity",
    www_authenticate_uri => "https://${api_fqdn}/identity",
    memcached_servers    => $memcached_servers,
    region_name          => $region_name,
  }

  class { '::barbican::api':
    default_transport_url      => os_transport_url({
      'transport' => 'rabbit',
      'hosts'     => fqdn_rotate($rabbit_hostnames),
      'port'      => '5671',
      'username'  => 'barbican',
      'password'  => $pass_barbican_messaging,
    }),
    host_href                   => "https://${api_fqdn}/keymanager",
    auth_strategy               => 'keystone',
    enabled_certificate_plugins => ['simple_certificate'],
    db_auto_create              => false,
    rabbit_use_ssl              => true,
    rabbit_ha_queues            => true,
    sync_db                     => true,
    max_allowed_secret_in_bytes => 25600,
  }
  # Fix the number of uwsgi processes
  class { '::barbican::wsgi::uwsgi': }
  # Disable uwsgi logs
  barbican_api_uwsgi_config {
    'uwsgi/disable-logging': value => true;
  }

  class { 'barbican::worker': }
}
