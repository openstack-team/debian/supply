# == Parameters:
#
# [*name*]
#   (Required) The name of VIP resource as per in corosync
#
# [*vip_ip*]
#   (Required) Ip address of the VIP to setup
#
# [*machine_ip*]
#   (Required) IP address where to bind corosync on.
#
# [*members_ip*]
#   (Required) Array of IP addresses to setup the VIP on.
#
class supply::services::vip(
  $vip_ip      = undef,
  $machine_ip  = undef,
  $members_ips = [],
  $members_ids = [],
){

  class { 'corosync':
    authkey                  => '/var/lib/puppet/ssl/certs/ca.pem',
    bind_address             => $machine_ip,
    cluster_name             => 'mycluster',
    enable_secauth           => true,
    set_votequorum           => true,
    quorum_members           => $members_ips,
    quorum_members_ids       => $members_ids,
    log_stderr               => false,
    log_function_name        => true,
    syslog_priority          => 'debug',
    debug                    => true,
    enable_totem_interface   => false,
  }
  corosync::service { 'pacemaker':
    version => '0',
  }->
  cs_property { 'stonith-enabled':
    value   => 'false',
  }->
  cs_property { 'no-quorum-policy':
    value   => 'stop',
  }

  cs_primitive { $name:
    primitive_class => 'ocf',
    primitive_type  => 'IPaddr2',
    provided_by     => 'heartbeat',
    parameters      => { 'ip' => $vip_ip, 'cidr_netmask' => 32, 'nic' => 'lo' },
    operations      => { 'monitor' => { 'interval' => '10s' } },
    before          => Anchor['keystone::service::begin'],
    require         => Cs_property['no-quorum-policy'],
  }

}
