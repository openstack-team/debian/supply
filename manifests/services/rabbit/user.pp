define supply::services::rabbit::user(
  $rabbit_password = undef,
  $beforeservice   = Anchor["${name}::service::begin"],
){
  rabbitmq_user { $name:
    admin    => true,
    password => $rabbit_password,
  }
  -> rabbitmq_user_permissions { "${name}@/":
    configure_permission => '.*',
    write_permission     => '.*',
    read_permission      => '.*',
    provider             => 'rabbitmqctl',
    require              => Class['::rabbitmq'],
    before               => $beforeservice,
  }
}