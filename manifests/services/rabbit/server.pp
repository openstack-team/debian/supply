class supply::services::rabbit::server(
  $machine_ip                      = undef,

  $pass_keystone_messaging         = undef,
  $pass_barbican_messaging         = undef,
  $pass_neutron_messaging          = undef,
  $pass_glance_messaging           = undef,
  $pass_nova_messaging             = undef,
  $pass_placement_messaging        = undef,
  $pass_ironic_messaging           = undef,
  $pass_ironic_inspector_messaging = undef,

  $pass_rabbitmq_monitoring        = undef,
  $pass_rabbitmq_cookie            = undef,

  $all_rabbits_ips                 = [],
  $all_rabbits_hostnames           = [],

  $has_glance                      = false,
  $has_neutron                     = false,
  $has_nova                        = false,
  $has_placement                   = false,
  $has_ironic                      = false,
){

  file { "/etc/rabbitmq/ssl/private":
    ensure                  => directory,
    owner                   => 'root',
    mode                    => '0755',
    require                 => File['/etc/rabbitmq/ssl'],
    selinux_ignore_defaults => true,
  }->
  file { "/etc/rabbitmq/ssl/public":
    ensure                  => directory,
    owner                   => 'root',
    mode                    => '0755',
    selinux_ignore_defaults => true,
  }->
  file { "/etc/rabbitmq/ssl/private/${::fqdn}.key":
    ensure                  => present,
    owner                   => "rabbitmq",
    source                  => "/etc/ssl/private/ssl-cert-snakeoil.key",
    selinux_ignore_defaults => true,
    mode                    => '0600',
  }->
  file { "/etc/rabbitmq/ssl/public/${::fqdn}.crt":
    ensure                  => present,
    owner                   => "rabbitmq",
    source                  => '/etc/ssl/certs/ssl-cert-snakeoil.pem',
    selinux_ignore_defaults => true,
    mode                    => '0644',
    notify        => Service['rabbitmq-server'],
  }

  class { '::rabbitmq':
    delete_guest_user           => true,
    node_ip_address             => $machine_ip,
    ssl_interface               => $machine_ip,
    ssl                         => true,
    ssl_only                    => true,
    ssl_cacert                  => '/etc/ssl/certs/ca-certificates.crt',
    ssl_cert                    => "/etc/rabbitmq/ssl/public/${::fqdn}.crt",
    ssl_key                     => "/etc/rabbitmq/ssl/private/${::fqdn}.key",
    environment_variables       => {},
    repos_ensure                => false,
    # Clustering options...
    config_cluster              => true,
    cluster_nodes               => $all_rabbits_ips,
    cluster_node_type           => 'ram',
    erlang_cookie               => $pass_rabbitmq_cookie,
    wipe_db_on_cookie_change    => true,
    collect_statistics_interval => 60000,
    cluster_partition_handling  => 'autoheal',
    config_variables            => {
        'vm_memory_high_watermark' => '0.4',
      },
  }->
  rabbitmq_vhost { '/':
    provider => 'rabbitmqctl',
    require  => Class['::rabbitmq'],
  }

  supply::services::rabbit::user { 'keystone': rabbit_password => $pass_keystone_messaging }
  supply::services::rabbit::user { 'barbican': rabbit_password => $pass_barbican_messaging }
  if $has_glance {
    supply::services::rabbit::user { 'glance': rabbit_password => $pass_glance_messaging }
  }

  if $has_nova {
    supply::services::rabbit::user { 'nova': rabbit_password => $pass_nova_messaging }
  }

  if $has_placement {
    supply::services::rabbit::user { 'placement': rabbit_password => $pass_placement_messaging }
  }

  if $has_neutron {
    supply::services::rabbit::user { 'neutron': rabbit_password => $pass_neutron_messaging }
  }

  if $has_ironic {
    supply::services::rabbit::user { 'ironic': rabbit_password => $pass_ironic_messaging }
    supply::services::rabbit::user { 'ironicinspector': rabbit_password => $pass_ironic_inspector_messaging, beforeservice => Anchor['ironic::service::begin'] }
  }
}
