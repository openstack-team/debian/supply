# == Parameters:
#
# [*api_key_pem*]
#   (Required) base64 encoded file for Key + Certificate of the API.
#
class supply::services::haproxy::server(
  $machine_ip                   = undef,
  $controller_ips               = [],
  $controller_hostnames         = [],

  $api_key_pem                  = undef,
  $pass_haproxy_stats           = undef,

  $haproxy_timeout_http_request = '10s',
  $haproxy_timeout_queue        = '1m',
  $haproxy_timeout_connect      = '10s',
  $haproxy_timeout_client       = '1m',
  $haproxy_timeout_server       = '1m',
  $haproxy_timeout_check        = '10s',

  $has_glance                   = false,
  $has_neutron                  = false,
  $has_nova                     = false,
  $has_placement                = false,
  $has_ironic                   = false,
){

  file { "/etc/haproxy/ssl":
    ensure                  => directory,
    owner                   => 'root',
    mode                    => '0755',
    selinux_ignore_defaults => true,
    require                 => Package['haproxy'],
  }->
  file { "/etc/haproxy/ssl/private":
    ensure                  => directory,
    owner                   => 'root',
    mode                    => '0755',
    selinux_ignore_defaults => true,
  }->
  file { "/etc/haproxy/ssl/private/supply-baremetal-api.pem":
    ensure                  => present,
    owner                   => "haproxy",
    content                 => base64('decode', $api_key_pem),
    selinux_ignore_defaults => true,
    mode                    => '0600',
    notify                  => Service['haproxy'],
  }

  class { 'haproxy':
    global_options   => {
      'log'     => '/dev/log local0',
      'chroot'  => '/var/lib/haproxy',
      'pidfile' => '/var/run/haproxy.pid',
      'maxconn' => '40960',
      'user'    => 'haproxy',
      'group'   => 'haproxy',
      'stats'   => [
        'socket /var/lib/haproxy/stats',
        'timeout 30s'
      ],
      'daemon'   => '',
      'nbthread' => '8',
      'ssl-default-bind-ciphers'   => 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256',
      'ssl-default-bind-options'   => 'no-sslv3 no-tlsv10 no-tlsv11 no-tls-tickets',
      'ssl-default-server-ciphers' => 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256',
      'ssl-default-server-options' => 'no-sslv3 no-tlsv10 no-tlsv11 no-tls-tickets',
    },
    defaults_options => {
      'log'     => 'global',
      'mode'      => 'http',
      'option'    => [
          'httplog',
        ],
      'retries' => '3',
      'timeout'   => [
          "http-request ${haproxy_timeout_http_request}",
          "queue ${haproxy_timeout_queue}",
          "connect ${haproxy_timeout_connect}",
          "client ${haproxy_timeout_client}",
          "server ${haproxy_timeout_server}",
          "check ${haproxy_timeout_check}",
        ],
      'maxconn' => '8000',
      'monitor-uri' => '/health'
    },
    merge_options => false,
# TODO: Fix scheduling.
#    before        => Anchor[$haproxy_schedule],
    require       => Sysctl::Value['net.ipv4.ip_nonlocal_bind'],
  }->
  haproxy::listen { 'hapstats':
    section_name => 'statshttp',
    bind         => { "${machine_ip}:8089" => 'user root'},
    mode         => 'http',
    options      => {
        'stats' => [ 'uri /', "auth admin:${pass_haproxy_stats}", 'refresh 15', 'admin if TRUE', 'realm Haproxy Statistics', ],
      },
  }->
  haproxy::frontend { 'health_check':
    section_name => 'healthcheck',
    bind         => { "${machine_ip}:8081" => 'user root'},
    mode         => 'http',
    options      => {
      timeout      => 'client 5s',
      monitor-uri  => '/',
    }
  }

  logrotate::rule { 'haproxy':
    path          => '/var/log/haproxy.log',
    rotate        => '7',
    rotate_every  => 'day',
    missingok     => true,
    compress      => true,
    delaycompress => true,
    postrotate    => '/usr/lib/rsyslog/rsyslog-rotate',
  }

  if $has_glance {
    $glance_opts = [
                     { 'acl'           => 'url_glance path_beg -i /image'},
                     { 'use_backend'   => 'glancebe if url_glance'},
                   ]
  }else{
    $glance_opts = []
  }

  $barbican_opts = [
                     { 'acl'           => 'url_barbican path_beg -i /keymanager'},
                     { 'use_backend'   => 'barbicanbe if url_barbican'},
                   ]


  if $has_neutron {
    $neutron_opts = [
                      { 'acl'           => 'url_neutron path_beg -i /network'},
                      { 'use_backend'   => 'neutronbe if url_neutron'},
                    ]
  }else{
    $neutron_opts = []
  }

  if $has_nova {
    $nova_opts = [
                   { 'acl'           => 'url_nova path_beg -i /compute'},
                   { 'use_backend'   => 'novabe if url_nova'},
                 ]
  }else{
    $nova_opts = []
  }

  if $has_placement {
    $placement_opts = [
                   { 'acl'           => 'url_placement path_beg -i /placement'},
                   { 'use_backend'   => 'placementbe if url_placement'},
                 ]
  }else{
    $placement_opts = []
  }

  if $has_ironic {
    $ironic_opts = [
                   { 'acl'           => 'url_ironic path_beg -i /baremetal'},
                   { 'use_backend'   => 'ironicbe if url_ironic'},
                   { 'acl'           => 'url_harddiscover path_beg -i /inspector'},
                   { 'use_backend'   => 'ironicinspectorbe if url_harddiscover'},
                 ]
  }else{
    $ironic_opts = []
  }

  $supply_def_opts = [
                       { 'http-response' => 'set-header Strict-Transport-Security max-age=63072000'},
                       { 'option'        => "forwardfor except ${machine_ip}"},
                       { 'acl'           => 'url_keystone path_beg -i /identity'},
                       { 'use_backend'   => 'keystonebe if url_keystone'},
                     ]

  $fe_all_opts = concat($supply_def_opts, $glance_opts, $barbican_opts, $neutron_opts, $nova_opts, $placement_opts, $ironic_opts)

  haproxy::frontend { 'openstackfe':
    mode      => 'http',
    bind      => {
                   "${machine_ip}:443" => ['ssl', 'crt', '/etc/haproxy/ssl/private/'],
                 },
    options   => $fe_all_opts,
  }

  ::supply::services::haproxy::backend { 'keystone':
    url_path          => 'identity',
    backend_ips       => $controller_ips,
    backend_hostnames => $controller_hostnames,
    backend_port      => 5000,
  }
  ::supply::services::haproxy::backend { 'barbican':
    url_path          => 'keymanager',
    backend_ips       => $controller_ips,
    backend_hostnames => $controller_hostnames,
    backend_port      => 9311,
  }

  if $has_glance {
    ::supply::services::haproxy::backend { 'glance':
      url_path          => 'image',
      backend_ips       => $controller_ips,
      backend_hostnames => $controller_hostnames,
      backend_port      => 9292,
      balance_mode      => 'source',
      use_ssl           => false,
    }
  }
  if $has_neutron {
    ::supply::services::haproxy::backend { 'neutron':
      url_path          => 'network',
      backend_ips       => $controller_ips,
      backend_hostnames => $controller_hostnames,
      backend_port      => 9696,
    }
  }
  if $has_nova {
    ::supply::services::haproxy::backend { 'nova':
      url_path          => 'compute',
      backend_ips       => $controller_ips,
      backend_hostnames => $controller_hostnames,
      backend_port      => 8774,
    }
    ::supply::services::haproxy::backend { 'novnc':
      url_path          => 'novnc',
      backend_ips       => $controller_ips,
      backend_hostnames => $controller_hostnames,
      backend_port      => 6080,
      balance_mode      => 'source',
    }
  }
  if $has_placement {
    ::supply::services::haproxy::backend { 'placement':
      url_path          => 'placement',
      healthcheck_path  => '/',
      backend_ips       => $controller_ips,
      backend_hostnames => $controller_hostnames,
      backend_port      => 8778,
    }
  }
  if $has_ironic {
    ::supply::services::haproxy::backend { 'ironic':
      url_path          => 'baremetal',
      healthcheck_path  => '/',
      backend_ips       => $controller_ips,
      backend_hostnames => $controller_hostnames,
      backend_port      => 6385,
    }
    ::supply::services::haproxy::backend { 'ironicinspector':
      healthcheck_path  => '/',
      url_path          => 'inspector',
      backend_ips       => $controller_ips,
      backend_hostnames => $controller_hostnames,
      backend_port      => 5050,
    }
  }
}
