define supply::services::haproxy::backend(
  $url_path          = undef,
  $backend_ips       = [],
  $backend_hostnames = [],
  $backend_port      = undef,
  $balance_mode      = 'roundrobin',
  $use_ssl           = true,
  $healthcheck_path  = '/healthcheck',
){

  if $use_ssl {
    $opts = 'check check-ssl ssl verify none'
  }else{
    $opts = 'check'
  }

  haproxy::backend { "${name}be":
    options => [
       { 'option'                       => 'forwardfor' },
       { 'option'                       => "httpchk GET ${healthcheck_path}" },
       { 'http-request'                 => "set-header X-Client-IP %[src]"},
       { 'mode'                         => 'http' },
       { 'balance'                      => $balance_mode },
       { 'http-request replace-path'    => "^/${url_path}/?(.*) /\1" },
       { 'http-response replace-header' => "Location: (https://[^/]*)/(.*)$ \1/${url_path}/\2" },
    ],
  }
  haproxy::balancermember { "${name}bm":
    listening_service => "${name}be",
    ipaddresses       => $backend_ips,
    server_names      => $backend_hostnames,
    ports             => $backend_port,
    options           => $opts,
  }
}
