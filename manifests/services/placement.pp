class supply::services::placement(
  $machine_ip             = undef,

  $pass_placement_messaging = undef,
  $pass_placement_db        = undef,
  $pass_placement_authtoken = undef,

  $controller_ips         = [],
  $rabbit_hostnames       = [],
  $sql_host               = undef,
  $api_fqdn               = undef,
  $region_name            = undef,
){

  ::supply::services::mysql::user { 'placement': mysql_password => $pass_placement_db }

  ::supply::sslkeypair {'placement': notify_service_name => 'placement-api' }

  class { '::placement':
    sync_db => true,
  }

  class { '::placement::keystone::auth':
    public_url   => "https://${api_fqdn}/placement",
    internal_url => "https://${api_fqdn}/placement",
    admin_url    => "https://${api_fqdn}/placement",
    password     => $pass_placement_authtoken,
    region       => $region_name,
  }

  class { '::placement::keystone::authtoken':
    password             => $pass_placement_authtoken,
    auth_url             => "https://${api_fqdn}/identity",
    www_authenticate_uri => "https://${api_fqdn}/identity",
    memcached_servers    => $memcached_servers,
    region_name          => $region_name,
  }
  class { '::placement::logging':
    debug => true,
  }
  class { '::placement::db':
    database_connection   => "mysql+pymysql://placement:${pass_placement_db}@${sql_host}/placementdb?charset=utf8",
  }
  package { 'python3-osc-placement':
    ensure => 'present',
  }
  class { '::placement::api':
    sync_db => $do_placement_db_sync,
  }
  # Fix the number of uwsgi processes
  class { '::placement::wsgi::uwsgi': }

  placement_api_uwsgi_config {
    'uwsgi/disable-logging': value => true;
  }

}
