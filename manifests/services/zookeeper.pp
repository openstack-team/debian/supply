# == Parameters:
#
# [*api_key_pem*]
#   (Required) base64 encoded file for Key + Certificate of the API.
#
class supply::services::zookeeper(
  $machine_ip = undef,
  $machine_id = undef,
  $members_ip = []
){

  # Hack to generate machine IDs automatically
  $members_ip.each |Integer $index, String $value| {
    if($machine_ip == $value){
      class { 'zookeeper':
        id                      => String($index + 1),
        client_ip               => $machine_ip,
        servers                 => $members_ip,
        purge_interval          => 6,
        max_allowed_connections => 600,
      }
    }
  }
}