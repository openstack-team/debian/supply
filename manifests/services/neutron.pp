class supply::services::neutron(
  $machine_ip             = undef,

  $pass_neutron_messaging = undef,
  $pass_neutron_db        = undef,
  $pass_neutron_authtoken = undef,
  $pass_nova_authtoken    = undef,
  $pass_ironic_authtoken  = undef,

  # This can be set to either ironic or ovs
  $backend_type           = 'ironic',

  $controller_ips         = [],
  $rabbit_hostnames       = [],
  $sql_host               = undef,
  $api_fqdn               = undef,
  $region_name            = undef,
){
  ::supply::services::mysql::user { 'neutron': mysql_password => $pass_neutron_db }

  ::supply::sslkeypair {'neutron': notify_service_name => 'neutron-api' }

  class { '::neutron::keystone::auth':
    public_url   => "https://${api_fqdn}/network",
    internal_url => "https://${api_fqdn}/network",
    admin_url    => "https://${api_fqdn}/network",
    password     => $pass_neutron_authtoken,
    region       => $region_name,
  }

  class { '::neutron':
    default_transport_url      => os_transport_url({
      'transport' => 'rabbit',
      'hosts'     => fqdn_rotate($rabbit_hostnames),
      'port'      => '5671',
      'username'  => 'neutron',
      'password'  => $pass_neutron_messaging,
    }),
    notification_transport_url => $neutron_notif_transport_url,
    rabbit_use_ssl             => true,
    rabbit_ha_queues           => true,
    kombu_ssl_ca_certs         => '/etc/ssl/certs/ca-certificates.crt',
    amqp_sasl_mechanisms       => 'PLAIN',
    core_plugin                => 'ml2',
    service_plugins            => ['router', 'metering', 'qos', 'trunk', 'segments', 'bgp'],
    bind_host                  => $machine_ip,
    use_ssl                    => true,
    cert_file                  => "/etc/neutron/ssl/public/${::fqdn}.crt",
    key_file                   => "/etc/neutron/ssl/private/${::fqdn}.pem",
    notification_driver        => 'noop',
    global_physnet_mtu         => 1500,
    # TODO: Fix this...
    dns_domain                 => 'example.com.',
    executor_thread_pool_size  => 512,
    # TODO: With designate, this must become ['notifications','notifications_designate']
    notification_topics        => ['notifications'],
  }

  neutron_config {
    'DEFAULT/rpc_conn_pool_size': value => $neutron_rpc_conn_pool_size;
  }
  class { '::neutron::logging':
    debug => true,
  }
  class { '::neutron::client': }

  $memcached_string = join([join($controller_ips,':11211,'), ':11211'],'')
  $memcached_servers  = ["${memcached_string}"]

  class { '::neutron::keystone::authtoken':
    password             => $pass_neutron_authtoken,
    auth_url             => "https://${api_fqdn}/identity",
    www_authenticate_uri => "https://${api_fqdn}/identity",
    memcached_servers    => $memcached_servers,
    region_name          => $region_name,
  }
  Service<| title == 'neutron-server'|> -> Openstacklib::Service_validation<| title == 'neutron-server' |> -> Neutron_network<||>


  class { 'neutron::db':
    database_connection              => "mysql+pymysql://neutron:${pass_neutron_db}@${sql_host}/neutrondb?charset=utf8",
    database_connection_recycle_time => 1800,
  }

  class { '::neutron::server':
    sync_db                          => true,
    api_workers                      => 2,
    rpc_workers                      => $::os_workers,
    validate                         => true,
    # TODO: understand both DVR and non-DVR
    router_distributed               => false,
    allow_automatic_l3agent_failover => true,
    enable_dvr                       => true,
    # TODO: Add dynamic-routing support
    ensure_dr_package                => false,
    service_providers                => [ 'LOADBALANCERV2:Octavia:neutron_lbaas.drivers.octavia.driver.OctaviaDriver:default', ],
    enable_proxy_headers_parsing     => true,
    max_l3_agents_per_router         => 2,
    l3_ha                            => true,
  }

  # Fix the number of uwsgi processes
  class { '::neutron::wsgi::uwsgi': }
  # Disable uwsgi logs
  neutron_api_uwsgi_config {
    'uwsgi/disable-logging': value => true;
  }

  class { '::neutron::server::placement':
    password            => $pass_neutron_authtoken,
    auth_type           => 'password',
    username            => 'neutron',
    project_domain_name => 'Default',
    project_name        => 'services',
    user_domain_name    => 'Default',
    auth_url            => "https://${api_fqdn}/identity",
    region_name         => $region_name,
  }

  if $backend_type == 'ironic' {
    class { '::neutron::agents::ml2::networking_baremetal':
      password             => $pass_ironic_authtoken,
      auth_url             => "https://${api_fqdn}/identity",
      region_name          => $region_name,
    }

    class { '::neutron::agents::l3':
      interface_driver      => 'openvswitch',
      debug                 => true,
      agent_mode            => 'legacy',
#      agent_mode            => $l3_agent_mode,
      ha_enabled            => false,
      extensions            => '',
    }

    neutron_plugin_ml2 {
      'securitygroup/firewall_driver': value => 'neutron.agent.linux.iptables_firewall.OVSHybridIptablesFirewallDriver';
    }

  } else {
    class { '::neutron::agents::ml2::ovs':
      local_ip                   => $machine_ip,
      tunnel_types               => ['vxlan'],
# TODO: make this NIC name more dynamic.
      bridge_uplinks             => ['ens3'],
#      bridge_uplinks             => ['eth0'],
      bridge_mappings            => [ 'external:br-ex' ],
#      bridge_mappings            => $bridge_mapping_list,
      extensions                 => '',
      l2_population              => true,
      arp_responder              => true,
      firewall_driver            => 'iptables_hybrid',
      drop_flows_on_start        => false,
#      enable_distributed_routing => $enable_distributed_routing,
      enable_distributed_routing => false,
      manage_vswitch             => false,
    }


    class { '::neutron::agents::l3':
      interface_driver      => 'openvswitch',
      debug                 => true,
      agent_mode            => 'legacy',
#      agent_mode            => $l3_agent_mode,
      ha_enabled            => false,
      extensions            => '',
      ha_vrrp_auth_type     => 'PASS',
      ha_vrrp_auth_password => $pass_neutron_vrrpauth,
    }
    neutron_l3_agent_config {
      'DEFAULT/external_network_bridge': value => '';
    }
  }
  if $backend_type == 'ironic' {
    $type_drivers = ['flat', ]
    $tenant_network_types = ['flat', ]
    $mechanism_drivers = 'openvswitch,baremetal'
  } else {
    $type_drivers = ['flat', 'vxlan', 'vlan', ]
    $tenant_network_types = ['flat', 'vxlan', 'vlan', ]
    $mechanism_drivers = 'openvswitch,l2population'
  }

  class { '::neutron::plugins::ml2':
    type_drivers          => $type_drivers,
    tenant_network_types  => $tenant_network_types,
#    extension_drivers     => $extension_drivers,
    extension_drivers     => 'port_security,qos',
    mechanism_drivers     => $mechanism_drivers,
    flat_networks         => [ 'external' ],
    network_vlan_ranges   => [ 'external' ],
#    flat_networks         => $external_network_list,
#    network_vlan_ranges   => $external_network_list,
    vni_ranges            => '1000:9999',
#    vni_ranges            => "${neutron_vxlan_vni_min}:${neutron_vxlan_vni_max}",
    path_mtu              => 1500,
    enable_security_group => true,
  }

  class { '::neutron::server::notifications::nova':
    auth_url    => "https://${api_fqdn}/identity",
    password    => $pass_nova_authtoken,
    region_name => $region_name,
  }

  class { '::neutron::agents::dhcp':
    interface_driver         => 'openvswitch',
    debug                    => true,
    enable_isolated_metadata => true,
    enable_metadata_network  => true,
    #dnsmasq_config_file
    #dnsmasq_dns_servers
    #dnsmasq_local_resolv
  }


  package { 'python3-neutron-dynamic-routing':
    ensure => present,
    notify => Service['neutron-api'],
  }

}
