# == Parameters:
#
# [*name*]
#   (Required) The name of VIP resource as per in corosync
#
# [*vip_ip*]
#   (Required) Ip address of the VIP to setup
#
# [*machine_ip*]
#   (Required) IP address where to bind corosync on.
#
# [*members_ip*]
#   (Required) Array of IP addresses to setup the VIP on.
#
class supply::services::firewall_controller(
  $vip_ip         = undef,
  $controller_ips = [],
){

  resources { "firewall":
    purge   => false
  }
  class { 'firewall': }

  $whitelist_all_ports = concat(['127.0.0.0/8'],$controller_ips)

  $whitelist_all_ports.each |Integer $index, String $value| {
    $val1 = $index+80
    firewall { "${val1} Allow ${value} to access everything":
      proto       => tcp,
      action      => accept,
      source      => "${value}",
    }
  }

  $whitelist_all_ports.each |Integer $index, String $value| {
    $val1 = $index+700
    firewall { "${val1} Allow ${value} to access OpenStack API port 443 without rate limit":
      proto       => tcp,
      action      => accept,
      source      => "${value}",
      dport       => [443],
    }
  }

}
