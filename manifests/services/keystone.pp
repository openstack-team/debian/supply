# == Parameters:
#
# [*api_key_pem*]
#   (Required) base64 encoded file for Key + Certificate of the API.
#
class supply::services::keystone(
  $pass_keystone_adminuser = undef,
  $pass_keystone_db        = undef,
  $pass_keystone_messaging = undef,
  $pass_keystone_credkey1  = undef,
  $pass_keystone_credkey2  = undef,
  $pass_keystone_fernkey1  = undef,
  $pass_keystone_fernkey2  = undef,

  $controller_ips          = [],
  $sql_host                = undef,
  $api_fqdn                = undef,
  $region_name             = undef,
){
  class { '::keystone::client': }

  ::supply::services::mysql::user { 'keystone': mysql_password => $pass_keystone_db }

  class { '::keystone::db':
    database_connection              => "mysql+pymysql://keystone:${pass_keystone_db}@${sql_host}/keystonedb",
    database_connection_recycle_time => 1800,
  }

  ::supply::sslkeypair {'keystone': notify_service_name => 'keystone' }

  class { '::keystone':
    catalog_driver             => 'sql',
    enabled                    => true,
    service_name               => 'keystone',
    enable_ssl                 => $use_ssl,
    manage_policyrcd           => false,
    enable_credential_setup    => true,
    credential_key_repository  => '/etc/keystone/credential-keys',
    credential_keys            => { '/etc/keystone/credential-keys/0' => { 'content' => $pass_keystone_credkey1 },
                                    '/etc/keystone/credential-keys/1' => { 'content' => $pass_keystone_credkey2 },
                                  },
    enable_fernet_setup        => true,
    fernet_replace_keys        => false,
    fernet_key_repository      => '/etc/keystone/fernet-keys',
    fernet_max_active_keys     => 4,
    fernet_keys                => { '/etc/keystone/fernet-keys/0' => { 'content' => base64('encode', $pass_keystone_fernkey1[0,32]) },
                                  },
    token_expiration           => 604800,
    public_endpoint            => "https://${api_fqdn}/identity",
    default_transport_url      => os_transport_url({
      'transport' => 'rabbit',
      'hosts'     => fqdn_rotate($controller_ips),
      'port'      => '5671',
      'username'  => 'keystone',
      'password'  => $pass_keystone_messaging,
    }),
    notification_transport_url => $keystone_notif_transport_url,
    notification_driver        => $notification_driver,
    rabbit_use_ssl             => $use_ssl,
    kombu_ssl_ca_certs         => $oci_pki_root_ca_file,
    rabbit_ha_queues           => true,
    require                    => Service['haproxy'],
    sync_db                    => true,
  }

  class { '::keystone::bootstrap':
    password   => $pass_keystone_adminuser,
    email      => 'root@example.com',
    admin_url  => "https://${api_fqdn}/identity",
    public_url => "https://${api_fqdn}/identity",
    region     => $region_name,
#    require    => Anchor['keystone::install::end'],
#    before     => Keystone_role['creator'],
  }
}
