class supply::services::glance(
  $pass_glance_messaging = undef,
  $pass_glance_db        = undef,
  $pass_glance_authtoken = undef,

  $controller_ips        = [],
  $sql_host              = undef,
  $api_fqdn              = undef,
  $region_name           = undef,
){

  ::supply::services::mysql::user { 'glance': mysql_password => $pass_glance_db }

  ::supply::sslkeypair {'glance':
    notify_service_name => 'glance-api',
  }

  include ::glance
  include ::glance::client

  class { '::glance::keystone::auth':
    public_url   => "https://${api_fqdn}/image",
    internal_url => "https://${api_fqdn}/image",
    admin_url    => "https://${api_fqdn}/image",
    password     => $pass_glance_authtoken,
    region       => $region_name,
  }

  $memcached_string = join([join($controller_ips,':11211,'), ':11211'],'')
  $memcached_servers  = ["${memcached_string}"]

  class { '::glance::api::authtoken':
    password             => $pass_glance_authtoken,
    auth_url             => "https://${api_fqdn}/identity",
    www_authenticate_uri => "https://${api_fqdn}/identity",
    memcached_servers    => $memcached_servers,
    region_name          => $region_name,
  }

  # Note: this only works on anything <= xena
  # after this, we should use ::glance::backend::multistore::file
  ::glance::backend::multistore::file {'supply-glance-file-backend': }

  class { '::glance::api::db':
    database_connection              => "mysql+pymysql://glance:${pass_glance_db}@${sql_host}/glancedb?charset=utf8",
    database_connection_recycle_time => 1800,
  }

  class { '::glance::api':
    workers                      => 2,
    bind_host                    => $machine_ip,
    sync_db                      => true,
    enable_proxy_headers_parsing => true,
    enabled_backends             => [ 'file:file' ],
    default_backend              => 'file',
    multi_store                  => true,
    public_endpoint              => "https://${api_fqdn}/image",
  }

  class { '::glance::api::logging':
    debug => true,
  }

}
