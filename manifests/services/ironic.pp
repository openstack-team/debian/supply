class supply::services::ironic(
  $machine_ip                      = undef,

  $pass_ironic_messaging           = undef,
  $pass_ironic_db                  = undef,
  $pass_ironic_authtoken           = undef,

  $pass_ironic_inspector_messaging = undef,
  $pass_ironic_inspector_db        = undef,
  $pass_ironic_inspector_authtoken = undef,

  $controller_ips                  = [],
  $rabbit_hostnames                = [],
  $sql_host                        = undef,
  $api_fqdn                        = undef,
  $region_name                     = undef,
){

  ::supply::services::mysql::user { 'ironic': mysql_password => $pass_ironic_db }
  class { "::ironic::inspector::db::mysql":
    dbname        => "ironicinspectordb",
    password      => $pass_ironic_inspector_db,
    allowed_hosts => '%',
    charset       => 'utf8mb3',
    collate       => 'utf8mb3_general_ci',
    require       => Anchor['mysql::server::end'],
    before        => Anchor["ironic-inspector::service::begin"],
  }

  ::supply::sslkeypair {'ironic': notify_service_name => 'ironic-api' }
  ::supply::sslkeypair {'ironic-inspector': notify_service_name => 'ironic-inspector-api' }

#  class { 'ironic::client': }

  class { '::ironic::keystone::auth':
    public_url   => "https://${api_fqdn}/baremetal",
    internal_url => "https://${api_fqdn}/baremetal",
    admin_url    => "https://${api_fqdn}/baremetal",
    password     => $pass_ironic_authtoken,
    region       => $region_name,
  }

  class { '::ironic::keystone::auth_inspector':
    public_url   => "https://${api_fqdn}/inspector",
    internal_url => "https://${api_fqdn}/inspector",
    admin_url    => "https://${api_fqdn}/inspector",
    password     => $pass_ironic_inspector_authtoken,
    region       => $region_name,
  }

  class { '::ironic::logging': debug => true, }

  class { '::ironic::db':
    database_connection => "mysql+pymysql://ironic:${pass_ironic_db}@${sql_host}/ironicdb?charset=utf8",
  }
  class { 'ironic::inspector::db':
    database_connection => "mysql+pymysql://ironic-inspector:${pass_ironic_inspector_db}@${sql_host}/ironicinspectordb?charset=utf8",
  }

  # These have to be declared before the main '::ironic'
  # (because of the later include).
  class { '::ironic::glance':
    password    => $pass_ironic_authtoken,
    auth_url    => "https://${api_fqdn}/identity",
    region_name => $region_name,
  }
  class { '::ironic::neutron':
    password    => $pass_ironic_authtoken,
    auth_url    => "https://${api_fqdn}/identity",
    region_name => $region_name,
  }
  class { '::ironic::nova':
    password    => $pass_ironic_authtoken,
    auth_url    => "https://${api_fqdn}/identity",
    region_name => $region_name,
  }

  class { '::ironic':
    default_transport_url => os_transport_url({
        'transport' => 'rabbit',
        'hosts'     => fqdn_rotate($rabbit_hostnames),
        'port'      => '5671',
        'username'  => 'ironic',
        'password'  => $pass_ironic_messaging,
      }),
#    notification_transport_url => $ironic_notif_transport_url,
#    notification_driver        => $ironic_notification_driver,
    enabled                    => true,
    my_ip                      => $machine_ip,
    auth_strategy              => 'keystone',
    rabbit_use_ssl             => true,
    rabbit_ha_queues           => true,
#    kombu_ssl_ca_certs         => $oci_pki_root_ca_file,
    amqp_sasl_mechanisms       => 'PLAIN',
#    http_root                  => '/var/www/html/boot',
  }

  class { '::ironic::api::authtoken':
    password             => $pass_ironic_authtoken,
    auth_url             => "https://${api_fqdn}/identity",
    www_authenticate_uri => "https://${api_fqdn}/identity",
    memcached_servers    => $memcached_servers,
    region_name          => $region_name,
  }

  class { '::ironic::inspector::logging': debug => true, }

  class { 'ironic::inspector::authtoken':
    password             => $pass_ironic_inspector_authtoken,
    auth_url             => "https://${api_fqdn}/identity",
    www_authenticate_uri => "https://${api_fqdn}/identity",
    memcached_servers    => $memcached_servers,
    region_name          => $region_name,
  }
  class { 'ironic::api':
    service_name                 => 'ironic-api',
    host_ip                      => $machine_ip,
    max_limit                    => 10000,
    enable_proxy_headers_parsing => true,
  }
  class { '::ironic::service_catalog':
    password             => $pass_ironic_authtoken,
    auth_url             => "https://${api_fqdn}/identity",
    region_name          => $region_name,
  }


  # TODO: write a ironic::wsgi::uwsgi class in puppet-ironic
  # to configure the number of API process automatically.
  class { '::ironic::conductor':
    enabled_hardware_types => [
#                                'idrac',
                                'ipmi',
#                                'ilo5',
#                                'redfish',
#                                'xclarity',
                              ],
    cleaning_disk_erase    => 'full',
    cleaning_network       => 'provisioning-net',
    inspection_network     => 'provisioning-net',
    provisioning_network   => 'provisioning-net',
    rescuing_network       => 'provisioning-net',
  }
  class { '::ironic::drivers::interfaces':
    enabled_management_interfaces => ['ipmitool'],
    enabled_boot_interfaces       => ['pxe'],
    enabled_deploy_interfaces     => ['direct'],
    enabled_power_interfaces      => ['ipmitool'],
    enabled_vendor_interfaces     => ['ipmitool', 'no-vendor'],
  }
  class { '::ironic::drivers::ipmi': }
  class { '::ironic::drivers::pxe':
    tftp_server          => $machine_ip,
    tftp_root            => '/srv/tftp',
    kernel_append_params => "boot=live iomem=relaxed console=tty0 console=ttyS0,115200 console=ttyS1,115200 earlyprintk=ttyS1,115200 consoleblank=0 systemd.show_status=true components fetch=http://${machine_ip}:8088/filesystem.squashfs",
  }

#  This class defiles twice file { '/srv/tftp': } so we can't use it.
#  Let's selectively copy some code from it instead.
#  class { '::ironic::pxe':
#    tftp_root => '/srv/tftp',
#    http_root => '/srv/tftp',
#  }

# We need apache to serve the above filesystem.squashfs.
  include apache
  apache::vhost { 'ipxe_vhost':
    priority => 10,
    options  => ['Indexes','FollowSymLinks'],
    docroot  => '/srv/tftp',
    port     => 8088,
  }

  class { '::ironic::drivers::inspector':
    password    => $pass_ironic_authtoken,
    auth_url    => "https://${api_fqdn}/identity",
    region_name => $region_name,
  }

  class { '::ironic::healthcheck': enabled => true }
#  class { '::ironic::inspector::policy': }
  class { '::ironic::inspector':
    listen_address        => $machine_ip,
    default_transport_url => os_transport_url({
        'transport' => 'rabbit',
        'hosts'     => fqdn_rotate($rabbit_hostnames),
        'port'      => '5671',
        'username'  => 'ironicinspector',
        'password'  => $pass_ironic_inspector_messaging,
      }),
    dnsmasq_interface        => 'ens3',
    tftp_root                => '/srv/tftp',
    node_not_found_hook      => 'enroll',
    discovery_default_driver => 'ipmi',
  }
  class { '::ironic::inspector::ironic':
    password             => $pass_ironic_authtoken,
    auth_url             => "https://${api_fqdn}/identity",
    endpoint_override    => "https://${api_fqdn}/baremetal",
    region_name          => $region_name,
  }
  class { '::ironic::inspector::service_catalog':
    password             => $pass_ironic_authtoken,
    auth_url             => "https://${api_fqdn}/identity",
    region_name          => $region_name,
  }
  ironic_inspector_config {
    'coordination/backend_url':  value => "memcached://${machine_ip}:11211";
    'oslo_messaging_rabbit/ssl': value => true;
  }

}
