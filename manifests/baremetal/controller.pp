class supply::baremetal::controller(
  $machine_ip                        = undef,
  $machine_hostname                  = undef,
  $region_name                       = 'RegionOne',

  $api_fqdn                          = undef,

  $controller_ips                    = [],
  $controller_hostnames              = [],
  $controller_ids                    = [],

  # Root CA certs
  $supply_pki_root_ca_cert           = undef,
  $supply_pki_supply_ca_cert         = undef,
  $supply_pki_supply_ca_chain        = undef,

  # Certs for the API
  $supply_pki_api_cert               = undef,
  $supply_pki_api_key                = undef,
  $supply_pki_api_pem                = undef,

  # Certificate for THIS machine
  $supply_pki_machine_cert           = undef,
  $supply_pki_machine_key            = undef,
  $supply_pki_machine_pem            = undef,

  $pass_mysql_rootuser               = undef,
  $pass_mysql_backup                 = undef,

  $pass_rabbitmq_cookie              = undef,
  $pass_rabbitmq_monitoring          = undef,

  $pass_keystone_adminuser           = undef,
  $pass_keystone_messaging           = undef,
  $pass_keystone_db                  = undef,
  $pass_keystone_credkey1            = undef,
  $pass_keystone_credkey2            = undef,
  $pass_keystone_fernkey1            = undef,
  $pass_keystone_fernkey2            = undef,

  $pass_barbican_messaging           = undef,
  $pass_barbican_db                  = undef,
  $pass_barbican_authtoken           = undef,

  $pass_metadata_proxy_shared_secret = undef,

  $pass_neutron_messaging            = undef,
  $pass_neutron_db                   = undef,
  $pass_neutron_authtoken            = undef,

  $pass_glance_messaging             = undef,
  $pass_glance_db                    = undef,
  $pass_glance_authtoken             = undef,

  $pass_ironic_messaging             = undef,
  $pass_ironic_db                    = undef,
  $pass_ironic_authtoken             = undef,

  $pass_ironic_inspector_messaging   = undef,
  $pass_ironic_inspector_db          = undef,
  $pass_ironic_inspector_authtoken   = undef,

  $pass_nova_messaging               = undef,
  $pass_nova_db                      = undef,
  $pass_novaapi_db                   = undef,
  $pass_nova_authtoken               = undef,
  $pass_nova_ssh_pub                 = undef,
  $pass_nova_ssh_priv                = undef,
  $nova_cpu_allocation_ratio         = 1,

  $pass_placement_messaging          = undef,
  $pass_placement_db                 = undef,
  $pass_placement_authtoken          = undef,

  $pass_haproxy_stats                = undef,

  $has_glance                        = false,
  $has_neutron                       = false,
  $has_nova                          = false,
  $has_placement                     = false,
  $has_ironic                        = false,

  # Parameters for clustering, only available if multinode is on.
  $multinode                         = false,
  $vip_ip                            = undef,
){

  alternatives { 'iptables':
    path => '/usr/sbin/iptables-legacy',
  }
  alternatives { 'ip6tables':
    path => '/usr/sbin/ip6tables-legacy',
  }

  ::supply::openrc {'supply':
    username     => 'admin',
    project_name => 'admin',
    password     => $pass_keystone_adminuser,
    auth_fqdn    => $api_fqdn,
    region_name  => $region_name,
  }

  class { '::supply::pki':
    supply_pki_root_ca_cert    => $supply_pki_root_ca_cert,
    supply_pki_supply_ca_cert  => $supply_pki_supply_ca_cert,
    supply_pki_supply_ca_chain => $supply_pki_supply_ca_chain,
    supply_pki_api_cert        => $supply_pki_api_cert,
    supply_pki_api_key         => $supply_pki_api_key,
    supply_pki_api_pem         => $supply_pki_api_pem,
    supply_pki_machine_cert    => $supply_pki_machine_cert,
    supply_pki_machine_key     => $supply_pki_machine_key,
    supply_pki_machine_pem     => $supply_pki_machine_pem,
  }

  class { '::supply::sysctl': }

  class { '::supply::services::mysql::server':
    machine_ip           => $machine_ip,
    machine_hostname     => $machine_hostname,

    first_master_ip      => $machine_ip,

    pass_mysql_rootuser  => $pass_mysql_rootuser,
    pass_mysql_backup    => $pass_mysql_backup,

    controller_ips       => $controller_ips,
    controller_hostnames => $controller_hostnames,
  }

  class { '::supply::services::haproxy::server':
    machine_ip           => $machine_ip,
    controller_ips       => $controller_ips,
    controller_hostnames => $controller_hostnames,
    api_key_pem          => $supply_pki_api_pem,
    pass_haproxy_stats   => $pass_haproxy_stats,

    has_glance           => $has_glance,
    has_neutron          => $has_neutron,
    has_nova             => $has_nova,
    has_placement        => $has_placement,
    has_ironic           => $has_ironic,
  }

  class { '::supply::services::rabbit::server':
    machine_ip                      => $machine_ip,
    pass_keystone_messaging         => $pass_keystone_messaging,
    pass_barbican_messaging         => $pass_barbican_messaging,
    pass_neutron_messaging          => $pass_neutron_messaging,
    pass_glance_messaging           => $pass_glance_messaging,
    pass_nova_messaging             => $pass_nova_messaging,
    pass_placement_messaging        => $pass_placement_messaging,
    pass_rabbitmq_monitoring        => $pass_rabbitmq_monitoring,
    pass_ironic_messaging           => $pass_ironic_messaging,
    pass_ironic_inspector_messaging => $pass_ironic_inspector_messaging,
    pass_rabbitmq_cookie            => $pass_rabbitmq_cookie,
    all_rabbits_ips                 => $controller_ips,
    all_rabbits_hostnames           => $controller_hostnames,

    has_glance                      => $has_glance,
    has_neutron                     => $has_neutron,
    has_nova                        => $has_nova,
    has_placement                   => $has_placement,
    has_ironic                      => $has_ironic,
  }

  class { '::supply::services::memcached':
    machine_ip => $machine_ip,
  }

  class { '::supply::services::keystone':
    pass_keystone_adminuser => $pass_keystone_adminuser,
    pass_keystone_db        => $pass_keystone_db,
    pass_keystone_messaging => $pass_keystone_messaging,
    pass_keystone_credkey1  => $pass_keystone_credkey1,
    pass_keystone_credkey2  => $pass_keystone_credkey2,
    pass_keystone_fernkey1  => $pass_keystone_fernkey1,
    pass_keystone_fernkey2  => $pass_keystone_fernkey2,

    controller_ips          => $controller_ips,
    sql_host                => $machine_ip,
    api_fqdn                => $api_fqdn,
    region_name             => $region_name,
  }

  class { '::supply::services::barbican':
    pass_barbican_messaging => $pass_barbican_messaging,
    pass_barbican_db        => $pass_barbican_db,
    pass_barbican_authtoken => $pass_barbican_authtoken,

    controller_ips          => $controller_ips,
    rabbit_hostnames        => $controller_hostnames,
    sql_host                => $machine_ip,
    api_fqdn                => $api_fqdn,
    region_name             => $region_name,
  }

  class { '::supply::services::glance':
    pass_glance_messaging => $pass_glance_messaging,
    pass_glance_db        => $pass_glance_db,
    pass_glance_authtoken => $pass_glance_authtoken,
    controller_ips        => $controller_ips,

    sql_host              => $machine_ip,
    api_fqdn              => $api_fqdn,
    region_name           => $region_name,
  }

  class { '::supply::services::nova::common':
    machine_ip                        => $machine_ip,
    pass_nova_messaging               => $pass_nova_messaging,
    pass_nova_db                      => $pass_nova_db,
    pass_novaapi_db                   => $pass_novaapi_db,
    pass_nova_authtoken               => $pass_nova_authtoken,
    pass_nova_ssh_pub                 => $pass_nova_ssh_pub,
    pass_nova_ssh_priv                => $pass_nova_ssh_priv,

    pass_neutron_authtoken            => $pass_neutron_authtoken,
    pass_placement_authtoken          => $pass_placement_authtoken,

    pass_metadata_proxy_shared_secret => $pass_metadata_proxy_shared_secret,

    controller_ips                    => $controller_ips,
    rabbit_hostnames                  => $controller_hostnames,
    sql_host                          => $machine_ip,
    api_fqdn                          => $api_fqdn,
    region_name                       => $region_name,
  }
  class { '::supply::services::nova::controller':
    machine_ip             => $machine_ip,
    sql_host               => $machine_ip,
    api_fqdn               => $api_fqdn,
    pass_nova_messaging    => $pass_nova_messaging,
    pass_nova_db           => $pass_nova_db,
    pass_novaapi_db        => $pass_novaapi_db,
    pass_nova_authtoken    => $pass_nova_authtoken,
  }
  class { '::supply::services::nova::compute':
    machine_ip            => $machine_ip,
    api_fqdn              => $api_fqdn,
    controller_ips        => $controller_ips,
    pass_ironic_authtoken => $pass_ironic_authtoken,
    region_name           => $region_name,
  }
  class { '::supply::services::ironic':
    machine_ip                      => $machine_ip,
    pass_ironic_messaging           => $pass_ironic_messaging,
    pass_ironic_db                  => $pass_ironic_db,
    pass_ironic_authtoken           => $pass_ironic_authtoken,
    pass_ironic_inspector_messaging => $pass_ironic_inspector_messaging,
    pass_ironic_inspector_db        => $pass_ironic_inspector_db,
    pass_ironic_inspector_authtoken => $pass_ironic_inspector_authtoken,
    controller_ips                  => $controller_ips,
    rabbit_hostnames                => $controller_hostnames,
    sql_host                        => $machine_ip,
    api_fqdn                        => $api_fqdn,
    region_name                     => $region_name,
  }
  class { '::supply::services::neutron':
    machine_ip             => $machine_ip,

    pass_neutron_messaging => $pass_neutron_messaging,
    pass_neutron_db        => $pass_neutron_db,
    pass_neutron_authtoken => $pass_neutron_authtoken,
    pass_nova_authtoken    => $pass_nova_messaging,
    pass_ironic_authtoken  => $pass_ironic_authtoken,

    controller_ips         => $controller_ips,
    rabbit_hostnames       => $controller_hostnames,
    sql_host               => $machine_ip,
    api_fqdn               => $api_fqdn,
    region_name            => $region_name,
  }

  class { '::supply::services::placement':
    machine_ip               => $machine_ip,

    pass_placement_messaging => $pass_placement_messaging,
    pass_placement_db        => $pass_placement_db,
    pass_placement_authtoken => $pass_placement_authtoken,

    controller_ips           => $controller_ips,
    rabbit_hostnames         => $controller_hostnames,
    sql_host                 => $machine_ip,
    api_fqdn                 => $api_fqdn,
    region_name              => $region_name,
  }

#  if $multinode {
#    ::supply::services::vip {'openstack-vip':
#      vip_ip         = $vip_ip,
#      machine_ip     = $machine_ip,
#      controller_ips = $controller_ips,
#      controller_ids = $controller_ids,
#    }
#  }

#  class { 'supply::services::firewall_controller':
#    vip_ip         => $vip_ip,
#    controller_ips => $controller_ips,
#  }

}
