# == Class: supply::deps
#
#  Nova anchors and dependency management
#
class supply::deps {

  if defined( Anchor['barbican::db::begin'] ){
    Class['haproxy'] -> Anchor['barbican::db::begin']
  }

  if defined( Anchor['heat::db::begin'] ){
    Class['haproxy'] -> Anchor['heat::db::begin']
  }

  if defined( Anchor['nova::db::begin'] ){
    Class['haproxy'] -> Anchor['nova::db::begin']
  }

  if defined( Anchor['neutron::db::begin'] ){
    Class['haproxy'] -> Anchor['neutron::db::begin']
  }

  if defined( Anchor['placement::db::begin'] ){
    Class['haproxy'] -> Anchor['placement::db::begin']
  }

  if defined( Anchor['cinder::db::begin'] ){
    Class['haproxy'] -> Anchor['cinder::db::begin']
  }

  if defined( Anchor['glance::db::begin'] ){
    Class['haproxy'] -> Anchor['glance::db::begin']
  }

  if defined( Anchor['aodh::db::begin'] ){
    Class['haproxy'] -> Anchor['aodh::db::begin']
  }

  if defined( Anchor['octavia::db::begin'] ){
    Class['haproxy'] -> Anchor['octavia::db::begin']
  }

  if defined( Anchor['panko::db::begin'] ){
    Class['haproxy'] -> Anchor['panko::db::begin']
  }

  if defined( Anchor['gnocchi::db::begin'] ){
    Class['haproxy'] -> Anchor['gnocchi::db::begin']
  }

  if defined( Anchor['cloudkitty::db::begin'] ){
    Class['haproxy'] -> Anchor['cloudkitty::db::begin']
  }

  if defined( Anchor['magnum::db::begin'] ){
    Class['haproxy'] -> Anchor['magnum::db::begin']
  }

  if defined( Anchor['designate::db::begin'] ){
    Class['haproxy'] -> Anchor['designate::db::begin']
  }

  if defined( Anchor['manila::db::begin'] ){
    Class['haproxy'] -> Anchor['manila::db::begin']
  }

  if defined( Anchor['ironic::db::begin'] ){
    Class['haproxy'] -> Anchor['ironic::db::begin']
  }

}
