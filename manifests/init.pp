# == Class: supply
#
# This class is used to specify configuration parameters that are common
# across all supply services.
#
# === Parameters:
#
# [*ensure_package*]
#   (optional) The state of nova packages
#   Defaults to 'present'
#
class supply(
  $ensure_package = 'present',
) inherits supply::params {

}
