class supply::pki(
  # Root CA certs
  $supply_pki_root_ca_cert    = undef,
  $supply_pki_supply_ca_cert  = undef,
  $supply_pki_supply_ca_chain = undef,

  # Certs for the API
  $supply_pki_api_cert        = undef,
  $supply_pki_api_key         = undef,
  $supply_pki_api_pem         = undef,

  # Certificate for THIS machine
  $supply_pki_machine_cert    = undef,
  $supply_pki_machine_key     = undef,
  $supply_pki_machine_pem     = undef,
){
  #######################################
  ### Manage the 2 root CAs of Supply ###
  #######################################
  file { '/usr/share/ca-certificates/supply':
    ensure                  => directory,
    owner                   => 'root',
    mode                    => '0755',
    selinux_ignore_defaults => true,
  }->
  # Root ca used to sign the supply ca
  file { '/usr/share/ca-certificates/supply/supply_1_selfsigned-root-ca.crt':
    ensure                  => present,
    owner                   => 'root',
    group                   => 'root',
    mode                    => '0644',
    selinux_ignore_defaults => true,
    content                 => base64('decode', $supply_pki_root_ca_cert),
  }->
  # OCI ca used to sign everything
  file { '/usr/share/ca-certificates/supply/supply_2_supply-ca.crt':
    ensure                  => present,
    owner                   => 'root',
    group                   => 'root',
    mode                    => '0644',
    selinux_ignore_defaults => true,
    content                 => base64('decode', $supply_pki_supply_ca_cert),
  }->
  file_line { 'ca-certificates-with-supply-1':
    path   => '/etc/ca-certificates.conf',
    match  => 'supply/supply_1_selfsigned-root-ca.crt',
    line   => 'supply/supply_1_selfsigned-root-ca.crt',
  }->
  file_line { 'ca-certificates-with-supply-2':
    path   => '/etc/ca-certificates.conf',
    match  => 'supply/supply_2_supply-ca.crt',
    line   => 'supply/supply_2_supply-ca.crt',
  }->
  exec { 'update-ca-certificates':
    command     => 'update-ca-certificates',
    logoutput   => 'on_failure',
    refreshonly => true,
    subscribe   => File['/usr/share/ca-certificates/supply/supply_1_selfsigned-root-ca.crt', '/usr/share/ca-certificates/supply/supply_2_supply-ca.crt'],
    path        => ['/usr/sbin', '/usr/bin', '/bin', '/sbin/', ],
  }

  ###############################
  ### Manage the machine cert ###
  ###############################
  file { '/etc/ssl/private/ssl-cert-snakeoil.key':
    ensure                  => present,
    owner                   => 'root',
    group                   => 'ssl-cert',
    mode                    => '0640',
    selinux_ignore_defaults => true,
    content                 => base64('decode', $supply_pki_machine_key),
  }
  file { '/etc/ssl/certs/ssl-cert-snakeoil.pem':
    ensure                  => present,
    owner                   => 'root',
    group                   => 'root',
    mode                    => '0644',
    selinux_ignore_defaults => true,
    content                 => base64('decode', $supply_pki_machine_cert),
  }

  ###########################
  ### Manage the API cert ###
  ###########################
  file {'/etc/ssl/private/supply-pki-api.key':
    ensure                  => present,
    owner                   => 'root',
    group                   => 'ssl-cert',
    mode                    => '0640',
    selinux_ignore_defaults => true,
    content                 => base64('decode', $supply_pki_api_key),
  }

  file {'/etc/ssl/certs/supply-pki-api.crt':
    ensure                  => present,
    owner                   => 'root',
    group                   => 'root',
    mode                    => '0644',
    selinux_ignore_defaults => true,
    content                 => base64('decode', $supply_pki_api_cert),
  }
  file {'/etc/ssl/certs/supply-pki-api.pem':
    ensure                  => present,
    owner                   => 'root',
    group                   => 'root',
    mode                    => '0644',
    selinux_ignore_defaults => true,
    content                 => base64('decode', $supply_pki_api_pem),
  }

}
