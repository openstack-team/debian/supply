#!/bin/sh

set -e

oslo-config-generator --output-file supply.conf \
  --namespace supplyopts \
  --namespace oslo.log \
  --namespace keystonemiddleware.auth_token
