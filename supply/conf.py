#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from oslo_config import cfg
from oslo_db import options as oslo_db_options
from oslo_log import log as logging
from oslo_middleware import cors
from oslo_middleware import http_proxy_to_wsgi
from oslo_policy import opts as policy_opts

base_supply_options = [
    cfg.StrOpt(
        'debian_mirror',
        default='http://deb.debian.org/debian',
        help='Debian mirror URL.'),
    cfg.StrOpt(
        'debian_security_mirror',
        default='http://security.debian.org/debian-security',
        help='Debian security mirror URL.'),
]



db_group = cfg.OptGroup('database', title='Supply API database options',
                                    help="""
The *Supply API Database* is a the database used with the supply
service. If the connection option is not set, the placement service will
not start.
""")

db_opts = [
    cfg.StrOpt(
        'connection',
        help='DSN.',
        default='mysql+pymysql://supply:tapasdsupply@localhost/supplydb',
        required=True,
        secret=True),
]

def register_opts(conf):
    conf.register_opts(base_supply_options)
    conf.register_opts(db_opts, group=db_group)

def list_opts():
    return [ ('DEFAULT', base_supply_options),
             (db_group,  db_opts), ]
