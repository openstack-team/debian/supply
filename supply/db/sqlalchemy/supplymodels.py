#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


from oslo_db.sqlalchemy import models
from oslo_log import log as logging
from sqlalchemy import Column
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Float
from sqlalchemy import ForeignKey
from sqlalchemy import Index
from sqlalchemy import Integer
from sqlalchemy import orm
from sqlalchemy import schema
from sqlalchemy import String
from sqlalchemy import Unicode
from sqlalchemy import Enum
from sqlalchemy import Boolean

LOG = logging.getLogger(__name__)


class _Base(models.ModelBase, models.TimestampMixin):
    pass


BASE = declarative_base()


class Clusters(BASE):
    __tablename__ = "clusters"
    __table_args__ = (
        schema.UniqueConstraint('name', name='uniq_cluster_name'),
        Index('clusters_uuid_idx', 'uuid'),
    )

    id = Column(Integer, primary_key=True, nullable=False)
    uuid = Column(String(36), nullable=False)
    name = Column(Unicode(200), nullable=True)
    domain_name = Column(Unicode(255), nullable=True)
    region_name = Column(Unicode(255), nullable=True)
    time_server_host = Column(Unicode(255), nullable=True)
    nameserver_v4_prim = Column(String(200), nullable=True)
    nameserver_v4_sec = Column(String(200), nullable=True)
    nameserver_v6_prim = Column(String(200), nullable=True)
    nameserver_v6_sec = Column(String(200), nullable=True)
    dhcp_domain = Column(Unicode(200), nullable=True)
    mail_relay_host = Column(Unicode(200), nullable=True)
    admin_email_address = Column(Unicode(200), nullable=True)


class Roles(BASE):
    __tablename__ = "roles"
    __table_args__ = (
        schema.UniqueConstraint('name', name='uniq_role_name'),
        Index('roles_uuid_idx', 'uuid'),
    )

    id = Column(Integer, primary_key=True, nullable=False)
    uuid = Column(String(36), nullable=False)
    name = Column(Unicode(64), nullable=False)


class Machines(BASE):
    __tablename__ = "machines"
    __table_args__ = (
        schema.UniqueConstraint('serial', name='uniq_serial'),
        Index('machines_uuid_idx', 'uuid'),
    )

    id = Column(Integer, primary_key=True, nullable=False)
    uuid = Column(String(36), nullable=False)
    hostname = Column(Unicode(255), nullable=True)

    cluster_id = Column(Integer, nullable=True)
    serial = Column(String(64), nullable=False)
    product_name = Column(String(64), nullable=False)
    vendor = Column(String(64), nullable=False)
 
    dhcp_ipv4 = Column(String(32), nullable=False, default='0.0.0.0')
 
    status = Column(Enum('live', 'installing', 'bootinglive', 'bootinghdd',
                         'prod'), nullable=False, server_default='live')

    cluster_id = orm.relationship(
        "Clusters",
        primaryjoin=('Machines.cluster_id == Clusters.id'),
        foreign_keys=cluster_id
    )

    role_id = orm.relationship(
        "Roles",
        primaryjoin=('Machines.role_id == Roles.id'),
        foreign_keys=cluster_id
    )
